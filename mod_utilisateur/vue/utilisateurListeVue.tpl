<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{$titre|upper}</title>

        <link rel="icon" type="image/png" href="public/images/plogo.PNG" />

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />

    </head>
    <body>
        {if $role eq 'Administrateur'}
            {include file="public/menu_Administrateur.tpl"}
        {/if}
        {if $role eq 'Modérateur'}
            {include file="public/menu_Moderateur.tpl"}
        {/if}
        {if $role eq 'Utilisateur'}
            {include file="public/menu_Utilisateur.tpl"}
        {/if}
        <div class="container-fluid">

            <div class="row mt-5">
                <div class="col-md-4 space">

                </div>
                <div class="col-md-6 space">
                    <h3>{$titreGestion}</h3>
                </div>
                <div class="col-md-2 space">

                    <form action='index.php' method='post'>
                        <input type='hidden' name='gestion' value='utilisateur'>
                        <input type='hidden' name='action' value='form_ajouter'>

                        Ajouter un Utilisateur :
                        <input type="submit"  class="btn btn-primary btn-sm mt-5"  name="ajouter" value="Ajouter">
                    </form>

                </div>
            </div>

            <div class="row">
                <div class="col-md-offset-1 col-md-10 col-md-offset-1">
                    <p>  {if $message neq ''} {$message}{/if}

                    </p>

                </div>
            </div>



            <div class="row">
                <!-- ICI LES DONNES  -->
                <div class="col-md-offset-1 col-md-10 col-md-offset-1">


                    <table class="table">
                        <thead class="">
                            <tr>
                                {*<th>
                                ID
                                </th>*}
                                <th>
                                    NOM
                                </th>
                                <th>
                                    PRENOM
                                </th>
                                <th>
                                    MAIL
                                </th>
                                <th>
                                    LOGIN
                                </th>

                                <th>
                                    ROLE
                                </th>
                                <th>
                                    ACTIONS
                                </th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <td colspan="6">
                                    {*  <p>INFORMATIONS : ... </p>*}
                                </td>
                            </tr>

                        </tfoot>
                        <tbody>

                            {foreach from=$listeUtilisateurs item=utilisateur}
                                <tr>
                                    {*<td>
                                    {$utilisateur.idUtilisateur}
                                    </td>*}
                                    <td>
                                        {$utilisateur.nomUtilisateur}
                                    </td>
                                    <td>
                                        {$utilisateur.prenomUtilisateur}
                                    </td>
                                    <td>
                                        {$utilisateur.mailUtilisateur}
                                    </td>
                                    <td>
                                        {$utilisateur.loginUtilisateur}
                                    </td>

                                    <td>
                                        {$utilisateur.typeUtilisateur}
                                    </td>

                                    <td>
                                        <form action='index.php' method='post'>
                                            <input type='hidden' name='idUtilisateur' value='{$utilisateur.idUtilisateur}'>
                                            <input type='hidden' name='gestion' value='utilisateur'>
                                            <input type='hidden' name='action' value='form_consulter'>

                                            <input type="submit"  class="btn btn-primary btn-sm rounded mb-1"   name="consulter" value="Consulter">
                                        </form>

                                        <form action='index.php' method='post'>
                                            <input type='hidden' name='idUtilisateur' value='{$utilisateur.idUtilisateur}'>
                                            <input type='hidden' name='gestion' value='utilisateur'>
                                            <input type='hidden' name='action' value='form_modifier'>

                                            <input type="submit"  class="btn btn-primary btn-sm rounded mb-1"   name="modifier" value="Modifier">
                                        </form>

                                        <form action='index.php' method='post'>
                                            <input type='hidden' name='idUtilisateur' value='{$utilisateur.idUtilisateur}'>
                                            <input type='hidden' name='gestion' value='utilisateur'>
                                            <input type='hidden' name='action' value='form_supprimer'>

                                            <input type="submit"  class="btn btn-primary btn-sm rounded mb-1"   name="supprimer" value="Supprimer">
                                        </form>
                                    </td>
                                </tr>
                            {foreachelse}
                                <tr>
                                    <td colspan='6'>Aucun enregistrement de trouvé.</td>
                                </tr>
                            {/foreach}

                        </tbody>
                    </table>
                </div>
            </div>

            {include file='public/piedPage.tpl'}

        </div>

        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


    </body>
</html>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{$titre|upper}</title>

        <link rel="icon" type="image/png" href="public/images/plogo.PNG" />
        <link href="public/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />

    </head>
    <body>
        {if $role eq 'Administrateur'}
            {include file="public/menu_Administrateur.tpl"}
        {/if}
        {if $role eq 'Modérateur'}
            {include file="public/menu_Moderateur.tpl"}
        {/if}
        {if $role eq 'Utilisateur'}
            {include file="public/menu_Utilisateur.tpl"}
        {/if}
        <div class="container-fluid mt-5">



            <div class="row">

                <div class=" container-fluid text-center p-0">
                    <h3>{$titreGestion}</h3>
                </div>
                <div class="col-md-2 space">

                </div>
            </div>

            <div class="row mt-5">

                <div class="col-md-offset-2 col-md-8 col-md-offset-2">

                    <p {if $unUtilisateur->getMessageErreur() neq ''} class="pos-messageErreur" {/if}>
                        {$unUtilisateur->getMessageErreur()}
                    </p>
                </div>

            </div>





            <!-- ICI LES DONNES, LE FORMULAIRE (LA FICHE !) -->
            <div class="container">
                <form action="index.php" method="post" novalidate="">

                    <input type="hidden" name="gestion" value="utilisateur">
                    <input type="hidden" name="action" value="{$action}">
                    <input id="motDePasseUtilisateur" name="motDePasseUtilisateur" type="hidden" value="">

                    {if $action neq 'ajouter'}
                        <div class="form-group">
                            <input class="form-control" id="idUtilisateur" name="idUtilisateur" type="hidden" value="{$unUtilisateur->getIdUtilisateur()}" readonly>
                        </div>
                    {/if}

                    <div class="form-group">
                        <label class="col-sm-2">Nom <sup>(*)</sup> :</label>
                        <strong>
                            <input class="form-control col-sm-16" id="nomUtilisateur" name="nomUtilisateur" type="text" value="{$unUtilisateur->getNomUtilisateur()}"  {$comportement} required="required">
                        </strong>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2">Prenom <sup>(*)</sup> :</label>
                        <strong>
                            <input class="form-control col-sm-16" id="prenomUtilisateur" name="prenomUtilisateur" type="text" value="{$unUtilisateur->getPrenomUtilisateur()}"  {$comportement} required="required">
                        </strong>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2"> Mail de l'utilisateur <sup>(*)</sup>:</label>
                        <input class="form-control col-sm-16" id="mailUtilisateur" name="mailUtilisateur" type="text" value="{$unUtilisateur->getMailUtilisateur()}" {$comportement} required="required">
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2">Login <sup>(*)</sup> :</label>
                        <input class="form-control col-sm-16" id="loginUtilisateur" name="loginUtilisateur" type="text" value="{$unUtilisateur->getLoginUtilisateur()}" {$comportement} required="required">
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2">  Mot de passe <sup>(*)</sup> :</label>
                        <input class="form-control col-sm-16" id="motDePasseUtilisateur" name="motDePasseUtilisateur" type="text" value="{$unUtilisateur->getMotDePasseUtilisateur()}" {$comportement} required="required">
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2"> Role:</label>
                        {if $action eq 'consulter' or $action eq 'supprimer'}
                            <input class="form-control col-sm-16" id="typeUtilisateur" name="typeUtilisateur" type="text" value="{$unUtilisateur->getTypeUtilisateur()}" {$comportement} >
                        {elseif $action eq 'ajouter'}

                            <select name="idTypeUtilisateur" class="col-sm-10 p-0" size="{$rolesAll|@count}">

                                {html_options  class="form-control" options=$rolesAll}
                            </select>

                        {else $action eq 'modifier'}
                            <select  name="idTypeUtilisateur" class="col-sm-10 p-0" size="{$rolesAll|@count}">
                                {html_options class="form-control"  options=$rolesAll selected=$roleSelected}
                            </select>

                        {/if}
                        {*      <input class="form-control" id="typeUtilisateur" name="typeUtilisateur" type="text" value="{$unUtilisateur->getTypeUtilisateur()}" {$comportement} required="required">
                        </div>*}

                    </div>

                    <div class="form-group">

                        <div class="col-sm-10 p-0">
                            <input type="button"  class="btn btn-warning btn-xs m-0"
                                   onclick='location.href = "index.php?gestion=utilisateur"' value="Retour">
                        </div>

                        {if $action neq 'consulter'}
                            <div class="col-sm-10 p-0">
                                <input id="delete"  type="submit" class="btn btn-warning btn-sm m-0 mt-2" value="{$action|capitalize}">

                            </div>
                        {/if}

                    </div>

                </form>

            </div>


            {include file='public/piedPage.tpl'}

        </div>
        <script src="public/js/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


    </body>
</html>

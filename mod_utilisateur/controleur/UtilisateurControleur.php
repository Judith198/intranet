<?php

class UtilisateurControleur {

    //put your code heprivate $parametre; //array
    private $oModele; // objet
    private $oVue; // objet

    public function __construct($parametre) {

        $this->parametre = $parametre;
//Création d'un objet modele
        $this->oModele = new UtilisateurModele($this->parametre);
//Création d'un objet vue
        $this->oVue = new UtilisateurVue($this->parametre);
    }

    public function liste() {

        $valeurs = $this->oModele->getListeUtilisateurs();

        $this->oVue->genererAffichageListe($valeurs);
    }

    public function form_consulter() {

        $valeurs = $this->oModele->getUnUtilisateur();

        $this->oVue->genererAffichageFiche($valeurs);
    }

    public function form_ajouter() {

        $prepareUtilisateur = new UtilisateurTable();
        $rolesAll = $this->getRolesForSelect($this->oModele->getAllRoles());
        $this->oVue->genererAffichageFiche($prepareUtilisateur, $rolesAll);
    }

    public function form_modifier() {

        $valeurs = $this->oModele->getUnUtilisateur();
        $rolesAll = $this->getRolesForSelect($this->oModele->getAllRoles());
        $this->oVue->genererAffichageFiche($valeurs, $rolesAll);
    }

    public function form_supprimer() {

        $valeurs = $this->oModele->getUnUtilisateur();

        $this->oVue->genererAffichageFiche($valeurs);
    }

    public function ajouter() {

        $controleUtilisateur = new UtilisateurTable($this->parametre);

        if ($controleUtilisateur->getAutorisationBD() == false) {
// ici nous sommes en erreur
            $rolesAll = $this->getRolesForSelect($this->oModele->getAllRoles());
            $this->oVue->genererAffichageFiche($controleUtilisateur, $rolesAll);
        } else {
// ici l'insertion est possible !
            $this->oModele->addUtilisateur($controleUtilisateur);
//Ici l'objet controleur (oControleur)
//Il a été créé dans le routeur
            $this->liste();
        }
    }

    public function modifier() {

        $controleUtilisateur = new UtilisateurTable($this->parametre);

        if ($controleUtilisateur->getAutorisationBD() == false) {
// ici nous sommes en erreur
            $rolesAll = $this->getRolesForSelect($this->oModele->getAllRoles());
            $this->oVue->genererAffichageFiche($controleUtilisateur, $rolesAll);
        } else {
// ici l'édition est possible !
            $this->oModele->editUtilisateur($controleUtilisateur);
//Ici l'objet controleur (oControleur)
//Il a été créé dans le routeur

            $this->liste();
        }
    }

    public function supprimer() {

        $this->oModele->deleteUtilisateur();

        $this->liste();
    }

    private function getRolesForSelect($rolesAll) {
        $tab = array();
        for ($i = 0; $i < count($rolesAll); $i++) {
            $role = $rolesAll[$i];
            $tab[$role['idTypeUtilisateur']] = $role['typeUtilisateur'];
        }
        return $tab;
    }

}

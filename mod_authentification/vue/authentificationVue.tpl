<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link href="public/css/style.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Libre+Baskerville:ital@1&display=swap" rel="stylesheet">
    </head>
    <body>
        <!------ Include the above in your HEAD tag ---------->

        <div class="wrapper fadeInDown p-0">
            <div class="container text-center mb-5" id="sliderWord">Bienvenue !
            </div>
            <div id="formContent">
                <!-- Tabs Titles -->
                <!-- Icon -->
                <div class="fadeIn first">
                    <img src="public/images/bibliogames.png" id="icon" alt="Logo bibliogames" />
                </div>

                <!-- Login Form -->
                <form role="form" action="index.php" method="POST" >
                    <input type="hidden" name="gestion" value="authentification">
                    <input type="hidden" name="action" value="{$action}">
                    <div class="row">

                        <div class="col-md-12 ">

                            <p {if $message neq ''} class="pos-messageErreur" {/if}>
                                {$message}
                            </p>

                        </div>

                    </div>
                    <input type="text" id="login" class="fadeIn second" name="f_login" placeholder="Identifiant" value="{$authentification->getF_login()}">
                    <input type="password" id="password" autocomplete="off" class="fadeIn third" name="f_motdepasse" placeholder="Mot de passe">
                    <input type="submit" class="fadeIn fourth" name="valider" value="Connexion">
                </form>
            </div>
        </div>
        <script src="public/js/custom.js" type="text/javascript"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    </body>
</html>



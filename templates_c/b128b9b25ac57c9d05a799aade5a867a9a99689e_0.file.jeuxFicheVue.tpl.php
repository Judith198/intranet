<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-20 13:40:32
  from 'C:\wamp64\www\bibliogames20avril\mod_jeux\vue\jeuxFicheVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e9da650f3d058_05776608',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b128b9b25ac57c9d05a799aade5a867a9a99689e' => 
    array (
      0 => 'C:\\wamp64\\www\\bibliogames20avril\\mod_jeux\\vue\\jeuxFicheVue.tpl',
      1 => 1587389847,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:public/menu_Administrateur.tpl' => 1,
    'file:public/menu_Moderateur.tpl' => 1,
    'file:public/menu_Utilisateur.tpl' => 1,
  ),
),false)) {
function content_5e9da650f3d058_05776608 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\bibliogames20avril\\include\\libs\\plugins\\modifier.capitalize.php','function'=>'smarty_modifier_capitalize',),));
?>
<!DOCTYPE html>     <
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">



        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />

    </head>
    <body>
        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Administrateur') {?>
            <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Administrateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Modérateur') {?>
            <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Moderateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Utilisateur') {?>
            <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Utilisateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
        <div class="container">






            <div class="row mt-5">


                <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Utilisateur') {?>
                    <div  class="mt-5 mr-4">
                        <form   id="formValidation">     <!--POUR VALIDATION FORM AVEC BIBLIO jQuery fonction validate()-->

                            <div class="form-group ">
                                <i class="fas fa-paper-plane  text-primary mb-2"></i>
                                <h4 class="text-uppercase ">Demande code d'activation</h4>
                                <hr  class="traitSousCode" style="border-color:  #C2185B">

                                <div class="form-group ">

                                </div>

                                <input type="text" class="form-control form-control2" placeholder="Nom" id="nom" name="contactNom"  required >
                                <div class="form-group ">

                                </div>
                                <input type="text" class="form-control form-control2" placeholder="Prénom" id="prenom"  name="contactPrenom" required >
                                <div class="form-group ">

                                </div>
                                <input type="email" class="form-control form-control2" placeholder="Adresse mail" id="email"   name="email"  >    
                            </div>



                            <div class="form-group ">
                                <textarea id="story" name="story"  placeholder="Message"
                                          rows="2" cols="50"></textarea>
                            </div>

                            <div class="form-group  couleurLettre">
                                <a id="send-msg" class="btn btn-primary"> Envoi </a>  
                            </div>

                        </form> 

                    </div>

                <?php }?>

                <div class="row  mt-5">
                    <!-- ICI LES DONNEES, LE FORMULAIRE (LA FICHE !) -->
                    <div class="col-ld-offset-2 col-md-12 col-ld-offset-2 ml-5 mt-5">
                        <form action="index.php" method="post" novalidate="">
                            <h1>FICHE JEU</h1>

                            <input type="hidden" name="gestion" value="jeux">
                            <input type="hidden" name="action" value="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">


                            <?php if ($_smarty_tpl->tpl_vars['action']->value != 'ajouter') {?>
                                <div class="form-group ">
                                    <label> Identifiant : </label>
                                    <input class="form-control pr-3 " id="idJeux" name="idJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getIdJeux();?>
" readonly>                                    </div>
                            <?php }?>

                            <div class="inputGroup-sizing-lg">
                                <div class="form-group ">
                                    <label> Nom du jeu  :</label>
                                    <strong>
                                        <input class="form-control" id="nomJeux" name="nomJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getNomJeux();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                                    </strong>
                                </div>

                                <div class="form-group">
                                    <label> Image du jeu :</label>

                                    <img src="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getImgJeux();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 >
                                    <?php if ($_smarty_tpl->tpl_vars['action']->value == 'modifier' || $_smarty_tpl->tpl_vars['action']->value == 'ajouter') {?>
                                        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Modérateur' || $_smarty_tpl->tpl_vars['role']->value == 'Administrateur') {?>   <input type="file">      <?php }?>
                                    <?php }?>
                                </div>

                                <div class="form-group">
                                    <label>  Année de sortie du jeu	:</label>
                                    <input class="form-control" id="anneeSortieJeux" name="anneeSortieJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getAnneeSortieJeux();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
>
                                </div>
                                <div class="form-group">
                                    <label> Description du jeu : </label>
                                    <input class="form-control" id="descriptionJeux"   name="descriptionJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getDescriptionJeux();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
>
                                </div>
                                                                <div class="form-group">

                                    <label>  PEGI :</label>
                                    <input class="form-control" id="agePegi"  name="agePegi" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getAgePegi();?>
"   <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 >
                                </div>
                                <div class="form-group">

                                    <label>   Type du jeu  :</label>
                                    <input class="form-control" id="libelleTypeJeux"  name="libelleTypeJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getLibelleTypeJeux();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 >
                                </div>


                                <div class="form-group">

                                    <label> Plateforme  :</label>
                                    <input class="form-control" id="libelleTypeJeux"  name="libelleTypeJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getNomPlateforme();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 >
                                </div>     


                                <div class="btn-group  mb-5">

                                    <div class="col-sm-10">
                                        <input type="button"  class="btn  btn-primary btn-sm"
                                               onclick='location.href = "index.php?gestion=jeux"' value="Retour">
                                    </div>

                                    <?php if ($_smarty_tpl->tpl_vars['action']->value != 'consulter') {?>
                                        <div class="col-md-1">
                                            <input type="submit" class="btn btn-primary btn-sm" value="<?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['action']->value);?>
">
                                        </div>
                                    <?php }?>

                                </div>


                            </div>
                    </div>

                </div>

                <?php echo '<script'; ?>
 src="public/js/jquery.min.js"><?php echo '</script'; ?>
>

                <?php echo '<script'; ?>
 src="public/js/custom.js" type="text/javascript"><?php echo '</script'; ?>
>
                <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"><?php echo '</script'; ?>
>
                <?php echo '<script'; ?>
 src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"><?php echo '</script'; ?>
>



                <?php echo '<script'; ?>
 src="public/js/ajaxEnvoiMail.js" type="text/javascript"><?php echo '</script'; ?>
>
                <?php echo '<script'; ?>
 src="public/js/jqueryvalidate.js" type="text/javascript"><?php echo '</script'; ?>
>    <!--BIBLIOTHEQUE JQUERY POUR VALIDATION FORMULAIRE -->
                <?php echo '<script'; ?>
 src="public/js/formvalidation.js" type="text/javascript"><?php echo '</script'; ?>
>

                <?php echo '<script'; ?>
>

                <?php echo '</script'; ?>
>
                </body>
                </html>
<?php }
}

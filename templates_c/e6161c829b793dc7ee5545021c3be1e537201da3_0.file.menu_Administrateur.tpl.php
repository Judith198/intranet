<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-20 13:40:26
  from 'C:\wamp64\www\bibliogames20avril\public\menu_Administrateur.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e9da64a2c3786_87874183',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e6161c829b793dc7ee5545021c3be1e537201da3' => 
    array (
      0 => 'C:\\wamp64\\www\\bibliogames20avril\\public\\menu_Administrateur.tpl',
      1 => 1587389848,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e9da64a2c3786_87874183 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top p-0">
    <div class="container-fluid p-1">
        <a class="navbar-brand" href="index.php?gestion=accueil">Accueil</a>
        <a class="navbar-brand" href="index.php?gestion=jeux">Jeux</a>
        <a class="navbar-brand" href="index.php?gestion=typeJeux">Type de jeux</a>
        <a class="navbar-brand" href="index.php?gestion=plateformes">Plateformes</a>
        <a class="navbar-brand" href="index.php?gestion=pegiJeux">Pegi</a>

        
        <div class="input-group d-flex justify-content-end">
            <div class="text-white align-self-center mr-2">Bonjour <?php echo $_smarty_tpl->tpl_vars['nomPrenom']->value;?>
 </div>
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Espace personnel
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="index.php?gestion=authentification&action=deconnexion"><?php echo $_smarty_tpl->tpl_vars['deconnexion']->value;?>
</a>
                    <a class="dropdown-item" href="index.php?gestion=utilisateur">Utilisateurs</a>
                </div>
            </div>
        </div>

    </div>
</nav>
<?php }
}

<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-20 13:40:31
  from 'C:\wamp64\www\bibliogames20avril\mod_jeux\vue\jeuxListeVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e9da64f11ab10_55655888',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '68d6844be4712a3c546edf5c4fb4854fff4769ab' => 
    array (
      0 => 'C:\\wamp64\\www\\bibliogames20avril\\mod_jeux\\vue\\jeuxListeVue.tpl',
      1 => 1587389847,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:public/menu_Administrateur.tpl' => 1,
    'file:public/menu_Moderateur.tpl' => 1,
    'file:public/menu_Utilisateur.tpl' => 1,
  ),
),false)) {
function content_5e9da64f11ab10_55655888 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['titre']->value, 'UTF-8');?>
</title>

        <!-- Bootstrap core CSS -->
        <!--<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->

        <!-- Custom styles for this template -->
        <!-- <link href="css/small-business.css" rel="stylesheet">-->

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />



    </head>

    <body>

        <!-- Navigation -->
        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Administrateur') {?>
            <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Administrateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Modérateur') {?>
            <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Moderateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Utilisateur') {?>
            <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Utilisateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>

        <div class="row">
            <div class="col-md-4 space">
                <a href="index.php?gestion=jeux"></a>
            </div>


        </div>


        <!-- Page Content -->
        <div class="container ">



            <div class = "mt-4 pt-4  col-lg-12">

               <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Modérateur' || $_smarty_tpl->tpl_vars['role']->value == 'Administrateur') {?>
                    <form    action='index.php' method='post'>
                        <input type='hidden' name='gestion' value='jeux'>
                        <input type='hidden' name='action' value='form_ajouter'>
                        <input type="submit"  class="btn btn-primary btn-sm rounded mb-1  mt-5"  name="ajouter" value="Ajouter">
                    </form>

                <?php }?>
            </div>
            
            <h1  class="mt-5">  Liste des jeux</h1>


            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listeJeux']->value, 'jeux');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['jeux']->value) {
?>
                <div class="esp"></div>
                <!-- Heading Row -->
                <div class="row align-items-center my-5 ">
                    <div class="col-lg-7">

                        <img  class="img-fluid rounded mb-4 mb-lg-0 " src="<?php echo $_smarty_tpl->tpl_vars['jeux']->value['imgJeux'];?>
"  alt="Image du jeu">
                    </div>
                    <!-- /.col-lg-8 -->
                    <div class="col-lg-5">
                        <h2 class="font-weight-light"><strong>  <?php echo $_smarty_tpl->tpl_vars['jeux']->value['nomJeux'];?>
</strong></h2>
                        <p> <?php echo $_smarty_tpl->tpl_vars['jeux']->value['descriptionJeux'];?>
</p>

                        <div class="btn-group">
                            <form action='index.php' method='post'>
                                <input type='hidden' name='idJeux' value='<?php echo $_smarty_tpl->tpl_vars['jeux']->value['idJeux'];?>
'>
                                <input type='hidden' name='gestion' value='jeux'>
                                <input type='hidden' name='action' value='form_consulter'>

                                <input type="submit"  class="btn btn-primary btn-sm rounded mb-1 mr-2"   name="consulter" value="Consulter">
                            </form>

                              <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Modérateur' || $_smarty_tpl->tpl_vars['role']->value == 'Administrateur') {?>  

                                <form action='index.php' method='post'>
                                    <input type='hidden' name='idJeux' value='<?php echo $_smarty_tpl->tpl_vars['jeux']->value['idJeux'];?>
'>
                                    <input type='hidden' name='gestion' value='jeux'>
                                    <input type='hidden' name='action' value='form_modifier'>

                                    <input type="submit"  class="btn btn-primary btn-sm rounded mb-1  mr-2"   name="modifier" value="Modifier">
                                </form>

                                <form action='index.php' method='post'>
                                    <input type='hidden' name='idJeux' value='<?php echo $_smarty_tpl->tpl_vars['jeux']->value['idJeux'];?>
'>
                                    <input type='hidden' name='gestion' value='jeux'>
                                    <input type='hidden' name='action' value='form_supprimer'>

                                    <input type="submit"  class="btn btn-primary btn-sm rounded mb-1"   name="supprimer" value="Supprimer">
                                </form>
                            <?php }?>
                        </div>
                    </div>
                    <!-- /.col-md-4 -->
                </div>
                <!-- /.row -->

            <?php
}
} else {
?>
                <tr>
                    <td colspan='6'>Aucun enregistrement de trouvé.</td>
                </tr>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>


        </div>
        <!-- /.row -->


        <!-- /.container -->

        <!-- Footer -->
        <footer class="py-5 bg-dark">
            <div class="container">
                <p class="m-0 text-center text-white">Copyright &copy; BIBLIOGAMES 2020</p>
            </div>
            <!-- /.container -->
        </footer>

        <!-- Bootstrap core JavaScript -->
        <!-- <?php echo '<script'; ?>
 src="vendor/jquery/jquery.min.js"><?php echo '</script'; ?>
>
         <?php echo '<script'; ?>
 src="vendor/bootstrap/js/bootstrap.bundle.min.js"><?php echo '</script'; ?>
> -->



        <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"><?php echo '</script'; ?>
>
    </body>

</html>
<?php }
}

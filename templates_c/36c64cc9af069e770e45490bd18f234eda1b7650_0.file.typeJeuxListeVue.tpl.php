<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-20 13:40:46
  from 'C:\wamp64\www\bibliogames20avril\mod_typeJeux\vue\typeJeuxListeVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e9da65e255b06_65020437',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '36c64cc9af069e770e45490bd18f234eda1b7650' => 
    array (
      0 => 'C:\\wamp64\\www\\bibliogames20avril\\mod_typeJeux\\vue\\typeJeuxListeVue.tpl',
      1 => 1587389848,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:public/menu_Administrateur.tpl' => 1,
    'file:public/menu_Moderateur.tpl' => 1,
    'file:public/menu_Utilisateur.tpl' => 1,
  ),
),false)) {
function content_5e9da65e255b06_65020437 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>  <h3><?php echo $_smarty_tpl->tpl_vars['titreGestion']->value;?>
</h3>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['titre']->value, 'UTF-8');?>
</title>


        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />

    </head>
    <body>
        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Administrateur') {?>
            <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Administrateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Modérateur') {?>
            <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Moderateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Utilisateur') {?>
            <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Utilisateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
        <div class="container-fluid">



            <div class="row">
                <div class="col-md-4 space">
                    <a href="index.php?gestion=typeJeux"></a>
                </div>
                <div class="col-md-6 space">

                </div>
                <div class="col-md-2 space">



                </div>
            </div>

            


            <div class="row">
                <!-- ICI LES DONNES  -->
                <div class="col-md-offset-1 col-md-10 col-md-offset-1">
                    <div class="text-center mt-5"><h1>Liste des types de jeux</h1></div>
                    <div class="row">
                        <div class="col-lg-9"></div>
                        <div class="pull-left col-xs-2"><p class='text-sm-left'>Ajouter un type de jeu :  </p></div>
                        <div class="col-lg-1">
                            <form action='index.php' method='post'>
                                <input type='hidden' name='gestion' value="typeJeux">
                                <input type='hidden' name='action' value='form_ajouter'>
                                <input type="submit"  class="btn btn-primary btn-sm rounded mb-1"   name="ajouter" value="Ajouter">
                            </form>
                        </div>

                    </div>

                    <table class="table  ml-5">
                        <thead class="">
                            <tr>
                                <th>
                                    Identifiant
                                </th>
                                <th>
                                    Type jeux
                                </th>

                            </tr>
                        </thead>

                        <tbody>

                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listeTypeJeux']->value, 'typeJeux');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['typeJeux']->value) {
?>
                                <tr>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['typeJeux']->value['idTypeJeux'];?>

                                    </td>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['typeJeux']->value['libelleTypeJeux'];?>

                                    </td>

                                    <td>

                                        <div class="btn-group">
                                            <form action='index.php' method='post'>
                                                <input type='hidden' name='idTypeJeux' value='<?php echo $_smarty_tpl->tpl_vars['typeJeux']->value['idTypeJeux'];?>
'>
                                                <input type='hidden' name='gestion' value='typeJeux'>
                                                <input type='hidden' name='action' value='form_modifier'>

                                                <input type="submit"  class="btn btn-primary btn-sm rounded mb-1 mr-2"   name="modifier" value="Modifier">
                                            </form>

                                            <form action='index.php' method='post'>
                                                <input type='hidden' name='idTypeJeux' value='<?php echo $_smarty_tpl->tpl_vars['typeJeux']->value['idTypeJeux'];?>
'>
                                                <input type='hidden' name='gestion' value='typeJeux'>
                                                <input type='hidden' name='action' value='form_supprimer'>

                                                <input type="submit"  class="btn btn-primary btn-sm rounded mb-1"   name="supprimer" value="Supprimer">
                                            </form>

                                        </div>
                                    </td>
                                </tr>
                            <?php
}
} else {
?>
                                <tr>
                                    <td colspan='6'>Aucun enregistrement de trouvé.</td>
                                </tr>
                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                        </tbody>
                    </table>
                </div>
            </div>



        </div>

        <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"><?php echo '</script'; ?>
>
    </body>
</html>
<?php }
}

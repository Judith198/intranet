<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-20 13:40:48
  from 'C:\wamp64\www\bibliogames20avril\mod_typeJeux\vue\typeJeuxFicheVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e9da660418d88_16153169',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3eca7780499ad6f3a4870744b9858f120689da16' => 
    array (
      0 => 'C:\\wamp64\\www\\bibliogames20avril\\mod_typeJeux\\vue\\typeJeuxFicheVue.tpl',
      1 => 1587389848,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:public/menu_Administrateur.tpl' => 1,
    'file:public/menu_Moderateur.tpl' => 1,
    'file:public/menu_Utilisateur.tpl' => 1,
  ),
),false)) {
function content_5e9da660418d88_16153169 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\bibliogames20avril\\include\\libs\\plugins\\modifier.capitalize.php','function'=>'smarty_modifier_capitalize',),));
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">



        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
    </head>
    <body>


        <div class="container-fluid">

            <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Administrateur') {?>
                <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Administrateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Modérateur') {?>
                <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Moderateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Utilisateur') {?>
                <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Utilisateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
            <?php }?>

            <div class="row">
                <div class="col-md-4 space">

                </div>

                <div class="col-md-4 space">

                </div>

                <div class="col-md-2 space">

                </div>
            </div>

            
        </div>




        <div class="row">
            <!-- ICI LES DONNEES, LE FORMULAIRE (LA FICHE !) -->
            <div class="col-md-offset-2 col-md-8 col-md-offset-2 space mt-5 ml-5">
                <form action="index.php" method="post" novalidate="">
                    <h1 class="mt-5 mb-5">FICHE TYPE JEU</h1>

                    <input type="hidden" name="gestion" value="typeJeux">
                    <input type="hidden" name="action" value="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">


                    <?php if ($_smarty_tpl->tpl_vars['action']->value != 'ajouter') {?>
                        <div class="form-group mt-5 ">
                            <label> Identifiant : </label>
                            <input class="form-control" id="idTypeJeux" name="idTypeJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unTypeJeux']->value->getIdTypeJeux();?>
" readonly>                            </div>
                    <?php }?>

                    <div class="form-group inputGroup-sizing-lg">
                        <label> Type du jeu :</label>
                        <strong>
                            <input class="form-control" id="libelleTypeJeux" name="libelleTypeJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unTypeJeux']->value->getLibelleTypeJeux();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                        </strong>
                    </div>



                    <div class="form-group">
                        <div class="col-sm-10 ">
                            <div class="btn-group">
                                <input type="button"  class="btn  btn-primary btn-sm  mr-2"
                                       onclick='location.href = "index.php?gestion=typeJeux"' value="Retour">
                                <form action='index.php' method='post'>
                                    <input type='hidden' name='gestion' value='typeJeux'>
                                
                                </form>


                                <?php if ($_smarty_tpl->tpl_vars['action']->value != 'consulter') {?>
                                    <div class="col-md-1">
                                        <input type="submit" class="btn btn-primary btn-sm" value="<?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['action']->value);?>
">
                                    </div>
                                <?php }?>      

                            </div>

                        </div>

                    </div>


                    <?php echo '<script'; ?>
 src="public/js/jquery.min.js"><?php echo '</script'; ?>
>

                    <?php echo '<script'; ?>
 src="public/js/custom.js" type="text/javascript"><?php echo '</script'; ?>
>
                    <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"><?php echo '</script'; ?>
>
                    <?php echo '<script'; ?>
 src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"><?php echo '</script'; ?>
>

                    </body>
                    </html>

<?php }
}

<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-20 13:40:16
  from 'C:\wamp64\www\bibliogames20avril\mod_authentification\vue\authentificationVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e9da640067386_50672128',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9f1866df581a5d03a8bad13164d42049fec11f18' => 
    array (
      0 => 'C:\\wamp64\\www\\bibliogames20avril\\mod_authentification\\vue\\authentificationVue.tpl',
      1 => 1587389847,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e9da640067386_50672128 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link href="public/css/style.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Libre+Baskerville:ital@1&display=swap" rel="stylesheet">
    </head>
    <body>
        <!------ Include the above in your HEAD tag ---------->

        <div class="wrapper fadeInDown p-0">
            <div class="container text-center mb-5" id="sliderWord">Bienvenue !
            </div>
            <div id="formContent">
                <!-- Tabs Titles -->
                <!-- Icon -->
                <div class="fadeIn first">
                    <img src="public/images/bibliogames.png" id="icon" alt="Logo bibliogames" />
                </div>

                <!-- Login Form -->
                <form role="form" action="index.php" method="POST" >
                    <input type="hidden" name="gestion" value="authentification">
                    <input type="hidden" name="action" value="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">
                    <div class="row">

                        <div class="col-md-12 ">

                            <p <?php if ($_smarty_tpl->tpl_vars['message']->value != '') {?> class="pos-messageErreur" <?php }?>>
                                <?php echo $_smarty_tpl->tpl_vars['message']->value;?>

                            </p>

                        </div>

                    </div>
                    <input type="text" id="login" class="fadeIn second" name="f_login" placeholder="Identifiant" value="<?php echo $_smarty_tpl->tpl_vars['authentification']->value->getF_login();?>
">
                    <input type="password" id="password" autocomplete="off" class="fadeIn third" name="f_motdepasse" placeholder="Mot de passe">
                    <input type="submit" class="fadeIn fourth" name="valider" value="Connexion">
                </form>
            </div>
        </div>
        <?php echo '<script'; ?>
 src="public/js/custom.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"><?php echo '</script'; ?>
>
    </body>
</html>


<?php }
}

<?php

class JeuxModele extends Modele {

    private $parametre;

    public function __construct($parametre) {

        $this->parametre = $parametre;
    }

    public function getListeJeux() {

        //Requête attendue de type SELECT (liste des typejeux)
//        $sql = "SELECT * FROM typejeux JOIN jeux ON typejeux.idTypeJeux = jeux.idTypeJeux JOIN pegi "
//                . "ON jeux.idPegi = pegi.idPegi JOIN plateformesJeux ON jeux.idJeux = plateformesJeux.idJeux JOIN plateformes ON plateformesJeux.idPlateforme = plateformes.idPlateforme  ";


        $sql = "SELECT * FROM typejeux JOIN jeux ON typejeux.idTypeJeux = jeux.idTypeJeux JOIN pegi "
                . "ON jeux.idPegi = pegi.idPegi ";

        $idRequete = $this->executeQuery($sql);

        return $idRequete->fetchall(PDO::FETCH_ASSOC);
    }

    public function getUnJeu() {

        //Requête attendue de type SELECT (un seul jeu)
   
          
        $sql = "SELECT * FROM plateformes JOIN plateformesjeux ON plateformes.idPlateforme = plateformesjeux.idPlateforme JOIN jeux ON "
                . "jeux.idJeux = plateformesjeux.idJeux JOIN pegi ON jeux.idPegi=pegi.idPegi JOIN typejeux ON jeux.idTypeJeux=typejeux.idTypeJeux WHERE jeux.idJeux = ?";

        $idRequete = $this->executeQuery($sql, array($this->parametre['idJeux']));

        //var_dump($idRequete->fetch());
        $jeu = new JeuxTable($idRequete->fetch());

        return $jeu;
    }

    public function addJeu(JeuxTable $valeurs) {
        // Requête de type Insert (création)
        
        $sql = "INSERT INTO  jeux ( nomJeux,imgJeux, anneeSortieJeux, descriptionJeux,codeActivationJeux,idPegi,idTypeJeux)
                 VALUES (?,'',?,?,'',(SELECT pegi.idPegi FROM pegi WHERE pegi.agePegi =?), (SELECT typejeux.idTypeJeux FROM typejeux WHERE typejeux.libelleTypeJeux = ?))
              ";
        
        $idRequete = $this->executeQuery($sql, array(
            $valeurs->getNomJeux(),
            $valeurs->getImgJeux(),
            $valeurs->getAnneeSortieJeux(),
            $valeurs->getDescriptionJeux(),
            $valeurs->getCodeActivationJeux(),
            $valeurs->getAgePegi(),
            $valeurs->getlibelleTypeJeux(),
//            $valeurs->getIdPegi(),
//            $valeurs->getIdTypeJeux(),
        ));

        if ($idRequete) {
            JeuxTable::setMessageSucces("Création effectuée avec succès !");
        }
    }

    public function editJeu(JeuxTable $valeurs) {
        // Requête de type Insert (création)
        $sql = "UPDATE jeux SET  nomJeux = ?, imgJeux = ?, anneeSortieJeux = ?, descriptionJeux = ?, codeActivationJeux =? 
			 WHERE idJeux = ?";


        $idRequete = $this->executeQuery($sql, array(
            $valeurs->getIdJeux(),
            $valeurs->getNomJeux(),
            $valeurs->getImgJeux(),
            $valeurs->getAnneeSortieJeux(),
            $valeurs->getDescriptionJeux(),
            $valeurs->getCodeActivationJeux(),
        ));

        if ($idRequete) {
            JeuxTable::setMessageSucces("Modification effectuée avec succès !");
        }
    }

    public function deleteJeu() {

        $sql = "DELETE FROM jeux WHERE idJeux = ?";


        $idRequete = $this->executeQuery($sql, array($this->parametre['idJeux']));

        if ($idRequete) {

            JeuxTable::setMessageSucces("Suppression effectuée avec succès !");
        }
    }

}

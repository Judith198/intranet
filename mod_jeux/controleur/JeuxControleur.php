<?php

/**
 * Description of lieuControleur
 *
 * @author tvosgiens
 */
class JeuxControleur {

    private $parametre; //array
    private $oModele; // objet
    private $oVue; // objet

    public function __construct($parametre) {

        $this->parametre = $parametre;
//Création d'un objet modele
        $this->oModele = new JeuxModele($this->parametre);
//Création d'un objet vue
        $this->oVue = new JeuxVue($this->parametre);
    }

    public function liste() {

        $valeurs = $this->oModele->getListeJeux();          

        $this->oVue->genererAffichageListe($valeurs);
    }

    public function form_consulter() {

        $valeurs = $this->oModele->getUnJeu();

        $this->oVue->genererAffichageFiche($valeurs);
    }

    public function form_ajouter() {            

        $preparejeu = new JeuxTable();

        $this->oVue->genererAffichageFiche($preparejeu);
    }

    public function form_modifier() {

        $valeurs = $this->oModele->getUnJeu();

        $this->oVue->genererAffichageFiche($valeurs);
    }

    public function form_supprimer() {

        $valeurs = $this->oModele->getUnJeu();

        $this->oVue->genererAffichageFiche($valeurs);
    }

    public function ajouter() {

        $controleJeu = new JeuxTable($this->parametre);

        if ($controleJeu->getAutorisationBD() == false) {
// ici nous sommes en erreur
            $this->oVue->genererAffichageFiche($controleJeu);
        } else {
// ici l'insertion est possible !
            $this->oModele->addJeu($controleJeu);
//Ici l'objet controleur (oControleur) 
//Il a été créé dans le routeur
            $this->liste();
        }
    }

    public function modifier() {

        $controleJeu = new JeuxTable($this->parametre);

        if ($controleJeu->getAutorisationBD() == false) {
// ici nous sommes en erreur
            $this->oVue->genererAffichageFiche($controleJeu);
        } else {
// ici l'édition est possible !
            $this->oModele->editJeu($controleJeu);
//Ici l'objet controleur (oControleur) 
//Il a été créé dans le routeur

            $this->liste();
        }
    }

    public function supprimer() {

        $this->oModele->deleteJeu();

        $this->liste();
    }

}

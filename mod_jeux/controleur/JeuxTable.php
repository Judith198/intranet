<?php

class JeuxTable {

    // 1 Déclarer les propriétés 
    private $idJeux = "";               //nom des champs de la table jeux
    private $nomJeux = "";
    private $imgJeux = "";
    private $anneeSortieJeux = "";
    private $descriptionJeux = "";
    private $codeActivationJeux = "";
    private $idPegi = "";
    private $agePegi = "";
    private $idTypeJeux = "";
    private $libelleTypeJeux = "";
    private $nomPlateforme = "";
    private $autorisationBD = true;
    private static $messageErreur = "";
    private static $messageSucces = "";

    // 2 Importer la méthode hydrater
    public function hydrater(array $row) {

        foreach ($row as $k => $v) {
            // Concaténation : nom de la méthode setter à appeler
            $setter = 'set' . ucfirst($k);
            // fonction prend 2 paramètres : l'objet en cours et le nom de la méthode
            if (method_exists($this, $setter)) {
                // Invoquer la méthode
                $this->$setter($v);
            }
        }
    }

    public function __construct($data = null) {

        if ($data != null) {

            $this->hydrater($data);
        }
    }

    // 3 Getters + Setters : ALT + INSERT
    // =========== GETTERS =================

    function getIdTypeJeux() {
        return $this->idTypeJeux;
    }

    function getIdJeux() {
        return $this->idJeux;
    }

    function getNomJeux() {
        return $this->nomJeux;
    }

    function getImgJeux() {
        return $this->imgJeux;
    }

    function getAnneeSortieJeux() {
        return $this->anneeSortieJeux;
    }

    function getDescriptionJeux() {
        return $this->descriptionJeux;
    }

    function getCodeActivationJeux() {
        return $this->codeActivationJeux;
    }

    function getIdPegi() {
        return $this->idPegi;
    }

    function getAgePegi() {
        return $this->agePegi;
    }

    function getLibelleTypeJeux() {
        return $this->libelleTypeJeux;
    }

    function getNomPlateforme() {
        return $this->nomPlateforme;
    }

    // =========== SETTERS =================

    function setIdTypeJeux($idTypeJeux) {
        $this->idTypeJeux = $idTypeJeux;
    }

    function setIdJeux($idJeux) {
        $this->idJeux = $idJeux;
    }

    function setNomJeux($nomJeux) {
        $this->nomJeux = $nomJeux;
    }

    function setImgJeux($imgJeux) {
        $this->imgJeux = $imgJeux;
    }

    function setAnneeSortieJeux($anneeSortieJeux) {
        $this->anneeSortieJeux = $anneeSortieJeux;
    }

    function setDescriptionJeux($descriptionJeux) {
        $this->descriptionJeux = $descriptionJeux;
    }

    function setCodeActivationJeux($codeActivationJeux) {
        $this->codeActivationJeux = $codeActivationJeux;
    }

    function setIdPegi($idPegi) {
        $this->idPegi = $idPegi;
    }

    function setAgePegi($agePegi) {
        $this->agePegi = $agePegi;
    }

    function setLibelleTypeJeux($libelleTypeJeux) {
        $this->libelleTypeJeux = $libelleTypeJeux;
    }

    function setNomPlateforme($nomPlateforme) {
        $this->nomPlateforme = $nomPlateforme;
    }

    /*     * *************AutorisationBD****************** */

    function getAutorisationBD() {
        return $this->autorisationBD;
    }

    function setAutorisationBD($autorisationBD) {
        $this->autorisationBD = $autorisationBD;
    }

    /*     * ********getMessageErreur ou getMessageSucces**************************** */

    public static function getMessageErreur() {
        return self::$messageErreur;
    }

    public static function getMessageSucces() {
        return self::$messageSucces;
    }

    public static function setMessageErreur($msg) {
        self::$messageErreur = self::$messageErreur . $msg . "<br>";
    }

    public static function setMessageSucces($msg) {
        self::$messageSucces = self::$messageSucces . $msg . "<br>";
    }

}

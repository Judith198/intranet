<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>{$titre|upper}</title>

        <!-- Bootstrap core CSS -->
        <!--<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->

        <!-- Custom styles for this template -->
        <!-- <link href="css/small-business.css" rel="stylesheet">-->

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />



    </head>

    <body>

        <!-- Navigation -->
        {if $role eq 'Administrateur'}
            {include file="public/menu_Administrateur.tpl"}
        {/if}
        {if $role eq 'Modérateur'}
            {include file="public/menu_Moderateur.tpl"}
        {/if}
        {if $role eq 'Utilisateur'}
            {include file="public/menu_Utilisateur.tpl"}
        {/if}

        <div class="row">
            <div class="col-md-4 space">
                <a href="index.php?gestion=jeux"></a>
            </div>


        </div>


        <!-- Page Content -->
        <div class="container ">



            <div class = "mt-4 pt-4  col-lg-12">

               {if $role eq 'Modérateur' OR $role eq 'Administrateur'}
                    <form    action='index.php' method='post'>
                        <input type='hidden' name='gestion' value='jeux'>
                        <input type='hidden' name='action' value='form_ajouter'>
                        <input type="submit"  class="btn btn-primary btn-sm rounded mb-1  mt-5"  name="ajouter" value="Ajouter">
                    </form>

                {/if}
            </div>
            
            <h1  class="mt-5">  Liste des jeux</h1>


            {foreach from=$listeJeux item=jeux}
                <div class="esp"></div>
                <!-- Heading Row -->
                <div class="row align-items-center my-5 ">
                    <div class="col-lg-7">

                        <img  class="img-fluid rounded mb-4 mb-lg-0 " src="{$jeux.imgJeux}"  alt="Image du jeu">
                    </div>
                    <!-- /.col-lg-8 -->
                    <div class="col-lg-5">
                        <h2 class="font-weight-light"><strong>  {$jeux.nomJeux}</strong></h2>
                        <p> {$jeux.descriptionJeux}</p>

                        <div class="btn-group">
                            <form action='index.php' method='post'>
                                <input type='hidden' name='idJeux' value='{$jeux.idJeux}'>
                                <input type='hidden' name='gestion' value='jeux'>
                                <input type='hidden' name='action' value='form_consulter'>

                                <input type="submit"  class="btn btn-primary btn-sm rounded mb-1 mr-2"   name="consulter" value="Consulter">
                            </form>

                              {if $role eq 'Modérateur' OR $role eq 'Administrateur'}  

                                <form action='index.php' method='post'>
                                    <input type='hidden' name='idJeux' value='{$jeux.idJeux}'>
                                    <input type='hidden' name='gestion' value='jeux'>
                                    <input type='hidden' name='action' value='form_modifier'>

                                    <input type="submit"  class="btn btn-primary btn-sm rounded mb-1  mr-2"   name="modifier" value="Modifier">
                                </form>

                                <form action='index.php' method='post'>
                                    <input type='hidden' name='idJeux' value='{$jeux.idJeux}'>
                                    <input type='hidden' name='gestion' value='jeux'>
                                    <input type='hidden' name='action' value='form_supprimer'>

                                    <input type="submit"  class="btn btn-primary btn-sm rounded mb-1"   name="supprimer" value="Supprimer">
                                </form>
                            {/if}
                        </div>
                    </div>
                    <!-- /.col-md-4 -->
                </div>
                <!-- /.row -->

            {foreachelse}
                <tr>
                    <td colspan='6'>Aucun enregistrement de trouvé.</td>
                </tr>
            {/foreach}


        </div>
        <!-- /.row -->


        <!-- /.container -->

        <!-- Footer -->
        <footer class="py-5 bg-dark">
            <div class="container">
                <p class="m-0 text-center text-white">Copyright &copy; BIBLIOGAMES 2020</p>
            </div>
            <!-- /.container -->
        </footer>

        <!-- Bootstrap core JavaScript -->
        <!-- <script src="vendor/jquery/jquery.min.js"></script>
         <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->



        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>

</html>

<!DOCTYPE html>     <
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">



        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />

    </head>
    <body>
        {if $role eq 'Administrateur'}
            {include file="public/menu_Administrateur.tpl"}
        {/if}
        {if $role eq 'Modérateur'}
            {include file="public/menu_Moderateur.tpl"}
        {/if}
        {if $role eq 'Utilisateur'}
            {include file="public/menu_Utilisateur.tpl"}
        {/if}
        <div class="container">






            <div class="row mt-5">


                {if $role eq 'Utilisateur'}
                    <div  class="mt-5 mr-4">
                        <form   id="formValidation">     <!--POUR VALIDATION FORM AVEC BIBLIO jQuery fonction validate()-->

                            <div class="form-group ">
                                <i class="fas fa-paper-plane  text-primary mb-2"></i>
                                <h4 class="text-uppercase ">Demande code d'activation</h4>
                                <hr  class="traitSousCode" style="border-color:  #C2185B">

                                <div class="form-group ">

                                </div>

                                <input type="text" class="form-control form-control2" placeholder="Nom" id="nom" name="contactNom"  required >
                                <div class="form-group ">

                                </div>
                                <input type="text" class="form-control form-control2" placeholder="Prénom" id="prenom"  name="contactPrenom" required >
                                <div class="form-group ">

                                </div>
                                <input type="email" class="form-control form-control2" placeholder="Adresse mail" id="email"   name="email"  >    
                            </div>



                            <div class="form-group ">
                                <textarea id="story" name="story"  placeholder="Message"
                                          rows="2" cols="50"></textarea>
                            </div>

                            <div class="form-group  couleurLettre">
                                <a id="send-msg" class="btn btn-primary"> Envoi </a>  
                            </div>

                        </form> 

                    </div>

                {/if}

                <div class="row  mt-5">
                    <!-- ICI LES DONNEES, LE FORMULAIRE (LA FICHE !) -->
                    <div class="col-ld-offset-2 col-md-12 col-ld-offset-2 ml-5 mt-5">
                        <form action="index.php" method="post" novalidate="">
                            <h1>FICHE JEU</h1>

                            <input type="hidden" name="gestion" value="jeux">
                            <input type="hidden" name="action" value="{$action}">


                            {if $action neq 'ajouter'}
                                <div class="form-group ">
                                    <label> Identifiant : </label>
                                    <input class="form-control pr-3 " id="idJeux" name="idJeux" type="text" value="{$unJeu->getIdJeux()}" readonly>    {*unJeu dans jeuxVue.php*}
                                </div>
                            {/if}

                            <div class="inputGroup-sizing-lg">
                                <div class="form-group ">
                                    <label> Nom du jeu  :</label>
                                    <strong>
                                        <input class="form-control" id="nomJeux" name="nomJeux" type="text" value="{$unJeu->getNomJeux()}"  {$comportement} required="required">
                                    </strong>
                                </div>

                                <div class="form-group">
                                    <label> Image du jeu :</label>

                                    <img src="{$unJeu->getImgJeux()}" {$comportement} >
                                    {if  $action eq 'modifier' OR $action eq 'ajouter'}
                                        {if $role eq 'Modérateur' OR $role eq 'Administrateur'}   <input type="file">      {/if}
                                    {/if}
                                </div>

                                <div class="form-group">
                                    <label>  Année de sortie du jeu	:</label>
                                    <input class="form-control" id="anneeSortieJeux" name="anneeSortieJeux" type="text" value="{$unJeu->getAnneeSortieJeux()}"  {$comportement}>
                                </div>
                                <div class="form-group">
                                    <label> Description du jeu : </label>
                                    <input class="form-control" id="descriptionJeux"   name="descriptionJeux" type="text" value="{$unJeu->getDescriptionJeux()}"  {$comportement}>
                                </div>
                                {*   <div class="form-group">
                                <label>   Code d'activation du jeu : </label>
                                <input class="form-control" id="codeActivationJeux" name="codeActivationJeux" type="text" value="{$unJeu->getCodeActivationJeux()}"  {$comportement}>
                                </div>
                                *}
                                <div class="form-group">

                                    <label>  PEGI :</label>
                                    <input class="form-control" id="agePegi"  name="agePegi" type="text" value="{$unJeu->getAgePegi()}"   {$comportement} >
                                </div>
                                <div class="form-group">

                                    <label>   Type du jeu  :</label>
                                    <input class="form-control" id="libelleTypeJeux"  name="libelleTypeJeux" type="text" value="{$unJeu->getLibelleTypeJeux()}"  {$comportement} >
                                </div>


                                <div class="form-group">

                                    <label> Plateforme  :</label>
                                    <input class="form-control" id="libelleTypeJeux"  name="libelleTypeJeux" type="text" value="{$unJeu->getNomPlateforme()}"  {$comportement} >
                                </div>     


                                <div class="btn-group  mb-5">

                                    <div class="col-sm-10">
                                        <input type="button"  class="btn  btn-primary btn-sm"
                                               onclick='location.href = "index.php?gestion=jeux"' value="Retour">
                                    </div>

                                    {if $action neq 'consulter'}
                                        <div class="col-md-1">
                                            <input type="submit" class="btn btn-primary btn-sm" value="{$action|capitalize}">
                                        </div>
                                    {/if}

                                </div>


                            </div>
                    </div>

                </div>

                <script src="public/js/jquery.min.js"></script>

                <script src="public/js/custom.js" type="text/javascript"></script>
                <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>



                <script src="public/js/ajaxEnvoiMail.js" type="text/javascript"></script>
                <script src="public/js/jqueryvalidate.js" type="text/javascript"></script>    <!--BIBLIOTHEQUE JQUERY POUR VALIDATION FORMULAIRE -->
                <script src="public/js/formvalidation.js" type="text/javascript"></script>

                <script>

                </script>
                </body>
                </html>

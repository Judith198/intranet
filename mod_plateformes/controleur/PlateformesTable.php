<?php


class PlateformesTable {

	// 1 Déclarer les propriétés 
	private $idPlateforme = "";               //nom des champs de la table typeJeux
	private $nomPlateforme = "";
	
	private $autorisationBD = true;
	private static $messageErreur = "";
	private static $messageSucces = "";

	// 2 Importer la méthode hydrater
	public function hydrater(array $row) {

		foreach ($row as $k => $v) {
			// Concaténation : nom de la méthode setter à appeler
			$setter = 'set' . ucfirst($k);
			// fonction prend 2 paramètres : l'objet en cours et le nom de la méthode
			if (method_exists($this, $setter)) {
				// Invoquer la méthode
				$this->$setter($v);
			}
		}
	}

	public function __construct($data = null) {

		if ($data != null) {

			$this->hydrater($data);
		}
	}

	// 3 Getters + Setters : ALT + INSERT
	// =========== GETTERS =================

        function getIdPlateforme() {
            return $this->idPlateforme;
        }

        function getNomPlateforme() {
            return $this->nomPlateforme;
        }

        
                
       
	// =========== SETTERS =================
        
        function setIdPlateforme($idPlateforme) {
            $this->idPlateforme = $idPlateforme;
        }

        

	function setNomPlateforme($nomPlateforme) {

		if (!is_string($nomPlateforme) || ctype_space($nomPlateforme) || empty($nomPlateforme)) {
			self::setMessageErreur("Le nom de la plateforme est invalide");
			$this->setAutorisationBD(false);
		}

		$this->nomPlateforme = $nomPlateforme;
	}
        

	/*	 * *************AutorisationBD****************** */

	function getAutorisationBD() {
		return $this->autorisationBD;
	}

	function setAutorisationBD($autorisationBD) {
		$this->autorisationBD = $autorisationBD;
	}

	/*	 * ********getMessageErreur ou getMessageSucces**************************** */

	public static function getMessageErreur() {
		return self::$messageErreur;
	}

	public static function getMessageSucces() {
		return self::$messageSucces;
	}

	public static function setMessageErreur($msg) {
		self::$messageErreur = self::$messageErreur . $msg . "<br>";
	}

	public static function setMessageSucces($msg) {
		self::$messageSucces = self::$messageSucces . $msg. "<br>";
	}

}

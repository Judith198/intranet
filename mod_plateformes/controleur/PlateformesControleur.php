<?php


class PlateformesControleur {

	private $parametre; //array
	private $oModele; // objet
	private $oVue; // objet

	public function __construct($parametre) {

		$this->parametre = $parametre;
//Création d'un objet modele
		$this->oModele = new PlateformesModele($this->parametre);
//Création d'un objet vue
		$this->oVue = new PlateformesVue($this->parametre);
	}

	public function liste() {

		$valeurs = $this->oModele->getListePlateformes();

		$this->oVue->genererAffichageListe($valeurs);
	}

	public function form_consulter() {

		$valeurs = $this->oModele->getUnePlateforme();

		$this->oVue->genererAffichageFiche($valeurs);
	}

	public function form_ajouter() {

		$preparePlateformes = new PlateformesTable();

		$this->oVue->genererAffichageFiche($preparePlateformes);
	}

	public function form_modifier() {

		$valeurs = $this->oModele->getUnePlateforme();

		$this->oVue->genererAffichageFiche($valeurs);
	}

	public function form_supprimer() {

		$valeurs = $this->oModele->getUnePlateforme();

		$this->oVue->genererAffichageFiche($valeurs);
	}

	public function ajouter() {

		$controlePlateformes = new PlateformesTable($this->parametre);

		if ($controlePlateformes->getAutorisationBD() == false) {
// ici nous sommes en erreur
			$this->oVue->genererAffichageFiche($controlePlateformes);
		} else {
// ici l'insertion est possible !
			$this->oModele->addPlateformes($controlePlateformes);
//Ici l'objet controleur (oControleur) 
//Il a été créé dans le routeur
			$this->liste();
		}
	}

	public function modifier() {

		$controlePlateformes = new PlateformesTable($this->parametre);

		if ($controlePlateformes->getAutorisationBD() == false) {
// ici nous sommes en erreur
			$this->oVue->genererAffichageFiche($controlePlateformes);
		} else {
// ici l'édition est possible !
			$this->oModele->editPlateformes($controlePlateformes);
//Ici l'objet controleur (oControleur) 
//Il a été créé dans le routeur

			$this->liste();
		}
	}

	public function supprimer() {

		$this->oModele->deletePlateformes();

		$this->liste();
	}

}

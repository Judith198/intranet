<?php

/**
 * Description of lieuVue
 *
 * @author tvosgiens
 */
class PlateformesVue {

    private $parametre; //array
    private $tpl; //objet
    private $valeurs;

    public function __construct($parametre) {

        $this->parametre = $parametre;

        $this->tpl = new Smarty();
    }

    public function chargementValeurs() {

        $this->tpl->assign('titre', 'Bibliogames');

        $this->tpl->assign('deconnexion', 'Déconnexion');

        $this->tpl->assign('piedPage', '');
        $this->tpl->assign('role', $_SESSION['role']);
        $this->tpl->assign('nomPrenom', $_SESSION['prenomNom']);
    }

    public function genererAffichageListe($valeurs) {

        $this->valeurs = $valeurs;

        $this->chargementValeurs();

        $this->tpl->assign('titreGestion', 'Liste des plateformes');

        //	$this->tpl->assign('message', typeJeuTab::getMessageSucces());

        $this->tpl->assign('listePlateformes', $this->valeurs);

        $this->tpl->display('mod_plateformes/vue/plateformesListeVue.tpl');
    }

    public function genererAffichageFiche($valeurs) {

        $this->valeurs = $valeurs;

        $this->chargementValeurs();

        switch ($this->parametre['action']) {

            case 'form_consulter':

                $this->tpl->assign('titreGestion', 'Consultation d\'un type de plateforme');

                $this->tpl->assign('action', 'consulter');

                $this->tpl->assign('comportement', 'disabled');

                $this->tpl->assign('unePlateforme', $this->valeurs);

                break;

            case 'form_ajouter':
            case 'ajouter':

                $this->tpl->assign('titreGestion', 'Création d\'un type de plateforme');

                $this->tpl->assign('action', 'ajouter');

                $this->tpl->assign('comportement', '');

                $this->tpl->assign('unePlateforme', $this->valeurs);

                break;

            case 'form_modifier':
            case 'modifier':

                $this->tpl->assign('titreGestion', 'Modification d\'un type de plateforme');

                $this->tpl->assign('action', 'modifier');

                $this->tpl->assign('comportement', '');

                $this->tpl->assign('unePlateforme', $this->valeurs);

                break;

            case 'form_supprimer':

                $this->tpl->assign('titreGestion', 'Suppression d\'un type de plateforme');

                $this->tpl->assign('action', 'supprimer');

                $this->tpl->assign('comportement', 'disabled');

                $this->tpl->assign('unePlateforme', $this->valeurs);

                break;
        }


        $this->tpl->display('mod_plateformes/vue/plateformesFicheVue.tpl');

        //Fin méthode
    }

//Fin classe
}

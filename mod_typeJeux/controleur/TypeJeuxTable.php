<?php


class TypeJeuxTable {

	// 1 Déclarer les propriétés 
	private $idTypeJeux = "";               //nom des champs de la table typeJeux
	private $libelleTypeJeux = "";
	
	private $autorisationBD = true;
	private static $messageErreur = "";
	private static $messageSucces = "";

	// 2 Importer la méthode hydrater
	public function hydrater(array $row) {

		foreach ($row as $k => $v) {
			// Concaténation : nom de la méthode setter à appeler
			$setter = 'set' . ucfirst($k);
			// fonction prend 2 paramètres : l'objet en cours et le nom de la méthode
			if (method_exists($this, $setter)) {
				// Invoquer la méthode
				$this->$setter($v);
			}
		}
	}

	public function __construct($data = null) {

		if ($data != null) {

			$this->hydrater($data);
		}
	}

	// 3 Getters + Setters : ALT + INSERT
	// =========== GETTERS =================

        function getIdTypeJeux() {
            return $this->idTypeJeux;
        }

        function getLibelleTypeJeux() {
            return $this->libelleTypeJeux;
        }

                
       
	// =========== SETTERS =================
        
        function setIdTypeJeux($idTypeJeux) {
            $this->idTypeJeux = $idTypeJeux;
        }


	function setLibelleTypeJeux($libelleTypeJeux) {

		if (!is_string($libelleTypeJeux) || ctype_space($libelleTypeJeux) || empty($libelleTypeJeux)) {
			self::setMessageErreur("Le nom du type de jeux est invalide");
			$this->setAutorisationBD(false);
		}

		$this->libelleTypeJeux = $libelleTypeJeux;
	}
        

	/*	 * *************AutorisationBD****************** */

	function getAutorisationBD() {
		return $this->autorisationBD;
	}

	function setAutorisationBD($autorisationBD) {
		$this->autorisationBD = $autorisationBD;
	}

	/*	 * ********getMessageErreur ou getMessageSucces**************************** */

	public static function getMessageErreur() {
		return self::$messageErreur;
	}

	public static function getMessageSucces() {
		return self::$messageSucces;
	}

	public static function setMessageErreur($msg) {
		self::$messageErreur = self::$messageErreur . $msg . "<br>";
	}

	public static function setMessageSucces($msg) {
		self::$messageSucces = self::$messageSucces . $msg. "<br>";
	}

}

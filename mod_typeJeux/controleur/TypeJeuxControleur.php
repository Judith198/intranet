<?php


class TypeJeuxControleur {

	private $parametre; //array
	private $oModele; // objet
	private $oVue; // objet

	public function __construct($parametre) {

		$this->parametre = $parametre;
//Création d'un objet modele
		$this->oModele = new TypeJeuxModele($this->parametre);
//Création d'un objet vue
		$this->oVue = new TypeJeuxVue($this->parametre);
	}

	public function liste() {

		$valeurs = $this->oModele->getListeTypeJeux();

		$this->oVue->genererAffichageListe($valeurs);
	}

	public function form_consulter() {

		$valeurs = $this->oModele->getUnTypeJeu();

		$this->oVue->genererAffichageFiche($valeurs);
	}

	public function form_ajouter() {

		$prepareJeu = new TypeJeuxTable();

		$this->oVue->genererAffichageFiche($prepareJeu);
	}

	public function form_modifier() {

		$valeurs = $this->oModele->getUnTypeJeu();

		$this->oVue->genererAffichageFiche($valeurs);
	}

	public function form_supprimer() {

		$valeurs = $this->oModele->getUnTypeJeu();

		$this->oVue->genererAffichageFiche($valeurs);
	}

	public function ajouter() {

		$controleTypeJeux = new TypeJeuxTable($this->parametre);

		if ($controleTypeJeux->getAutorisationBD() == false) {
// ici nous sommes en erreur
			$this->oVue->genererAffichageFiche($controleTypeJeux);
		} else {
// ici l'insertion est possible !
			$this->oModele->addTypeJeux($controleTypeJeux);
//Ici l'objet controleur (oControleur) 
//Il a été créé dans le routeur
			$this->liste();
		}
	}

	public function modifier() {

		$controleTypeJeux = new TypeJeuxTable($this->parametre);

		if ($controleTypeJeux->getAutorisationBD() == false) {
// ici nous sommes en erreur
			$this->oVue->genererAffichageFiche($controleTypeJeux);
		} else {
// ici l'édition est possible !
			$this->oModele->editTypeJeux($controleTypeJeux);
//Ici l'objet controleur (oControleur) 
//Il a été créé dans le routeur

			$this->liste();
		}
	}

	public function supprimer() {

		$this->oModele->deleteTypeJeux();

		$this->liste();
	}

}

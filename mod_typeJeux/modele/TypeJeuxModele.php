<?php

/**
 * Description of lieuModele
 *
 * @author tvosgiens
 */
class TypeJeuxModele extends Modele {

    private $parametre;

    public function __construct($parametre) {

        $this->parametre = $parametre;
    }

    public function getListeTypeJeux() {

        //Requête attendue de type SELECT (liste des typejeux)
        $sql = "SELECT * FROM typejeux";

        $idRequete = $this->executeQuery($sql);

        return $idRequete->fetchall(PDO::FETCH_ASSOC);
    }

    public function getUnTypeJeu() {

        //Requête attendue de type SELECT (un seul lieu)
        $sql = "SELECT * FROM typejeux WHERE idTypeJeux = ?";

        $idRequete = $this->executeQuery($sql, array($this->parametre['idTypeJeux']));

        //var_dump($idRequete->fetch());
        $typeJeu = new TypeJeuxTable($idRequete->fetch());

        return $typeJeu;
    }

    public function addTypeJeux(TypeJeuxTable $valeurs) {
        // Requête de type Insert (création)
        $sql = "INSERT INTO  typejeux (libelleTypeJeux)
				  VALUES (?)";

        $idRequete = $this->executeQuery($sql, array(
            $valeurs->getLibelleTypeJeux()
        ));

        if ($idRequete) {
            TypeJeuxTable::setMessageSucces("Création effectuée avec succès !");
        }
    }

    public function editTypeJeux(TypeJeuxTable $valeurs) {
        // Requête de type Insert (création)
        $sql = "UPDATE typejeux SET libelleTypeJeux = ?
			 WHERE idTypeJeux = ?";


        $idRequete = $this->executeQuery($sql, array(
            $valeurs->getLibelleTypeJeux(),
            $valeurs->getIdTypeJeux()
        ));

        if ($idRequete) {
            TypeJeuxTable::setMessageSucces("Modification effectuée avec succès !");
        }
    }

    public function deleteTypeJeux() {

        $sql = "DELETE FROM typejeux WHERE idTypeJeux = ?";


        $idRequete = $this->executeQuery($sql, array($this->parametre['idTypeJeux']));

        if ($idRequete) {

            TypeJeuxTable::setMessageSucces("Suppression effectuée avec succès !");
        }
    }

}

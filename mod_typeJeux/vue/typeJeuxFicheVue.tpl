<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">



        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
    </head>
    <body>


        <div class="container-fluid">

            {if $role eq 'Administrateur'}
                {include file="public/menu_Administrateur.tpl"}
            {/if}
            {if $role eq 'Modérateur'}
                {include file="public/menu_Moderateur.tpl"}
            {/if}
            {if $role eq 'Utilisateur'}
                {include file="public/menu_Utilisateur.tpl"}
            {/if}

            <div class="row">
                <div class="col-md-4 space">

                </div>

                <div class="col-md-4 space">

                </div>

                <div class="col-md-2 space">

                </div>
            </div>

            {*      <div class="row">

            {*     <div class="col-md-offset-2 col-md-8 col-md-offset-2">

            {*           <p {if $lePegiJeux->getMessageErreur() neq ''} class="pos-messageErreur" {/if}>
            {$lePegiJeux->getMessageErreur()}
            </p>
            </div>    *}

        </div>




        <div class="row">
            <!-- ICI LES DONNEES, LE FORMULAIRE (LA FICHE !) -->
            <div class="col-md-offset-2 col-md-8 col-md-offset-2 space mt-5 ml-5">
                <form action="index.php" method="post" novalidate="">
                    <h1 class="mt-5 mb-5">FICHE TYPE JEU</h1>

                    <input type="hidden" name="gestion" value="typeJeux">
                    <input type="hidden" name="action" value="{$action}">


                    {if $action neq 'ajouter'}
                        <div class="form-group mt-5 ">
                            <label> Identifiant : </label>
                            <input class="form-control" id="idTypeJeux" name="idTypeJeux" type="text" value="{$unTypeJeux->getIdTypeJeux()}" readonly>    {*unJeu dans jeuxVue.php*}
                        </div>
                    {/if}

                    <div class="form-group inputGroup-sizing-lg">
                        <label> Type du jeu :</label>
                        <strong>
                            <input class="form-control" id="libelleTypeJeux" name="libelleTypeJeux" type="text" value="{$unTypeJeux->getLibelleTypeJeux()}"  {$comportement} required="required">
                        </strong>
                    </div>



                    <div class="form-group">
                        <div class="col-sm-10 ">
                            <div class="btn-group">
                                <input type="button"  class="btn  btn-primary btn-sm  mr-2"
                                       onclick='location.href = "index.php?gestion=typeJeux"' value="Retour">
                                <form action='index.php' method='post'>
                                    <input type='hidden' name='gestion' value='typeJeux'>
                                
                                </form>


                                {if $action neq 'consulter'}
                                    <div class="col-md-1">
                                        <input type="submit" class="btn btn-primary btn-sm" value="{$action|capitalize}">
                                    </div>
                                {/if}      

                            </div>

                        </div>

                    </div>


                    <script src="public/js/jquery.min.js"></script>

                    <script src="public/js/custom.js" type="text/javascript"></script>
                    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
                    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

                    </body>
                    </html>


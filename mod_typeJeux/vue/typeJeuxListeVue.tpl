<!DOCTYPE html>  <h3>{$titreGestion}</h3>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{$titre|upper}</title>


        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />

    </head>
    <body>
        {if $role eq 'Administrateur'}
            {include file="public/menu_Administrateur.tpl"}
        {/if}
        {if $role eq 'Modérateur'}
            {include file="public/menu_Moderateur.tpl"}
        {/if}
        {if $role eq 'Utilisateur'}
            {include file="public/menu_Utilisateur.tpl"}
        {/if}
        <div class="container-fluid">



            <div class="row">
                <div class="col-md-4 space">
                    <a href="index.php?gestion=typeJeux"></a>
                </div>
                <div class="col-md-6 space">

                </div>
                <div class="col-md-2 space">



                </div>
            </div>

            {*		<div class="row">
            <div class="col-md-offset-1 col-md-10 col-md-offset-1">
            <p {if $message neq ''} class='pos-message'{/if}>
            {$message}
            </p>

            </div>
            </div>		*}



            <div class="row">
                <!-- ICI LES DONNES  -->
                <div class="col-md-offset-1 col-md-10 col-md-offset-1">
                    <div class="text-center mt-5"><h1>Liste des types de jeux</h1></div>
                    <div class="row">
                        <div class="col-lg-9"></div>
                        <div class="pull-left col-xs-2"><p class='text-sm-left'>Ajouter un type de jeu :  </p></div>
                        <div class="col-lg-1">
                            <form action='index.php' method='post'>
                                <input type='hidden' name='gestion' value="typeJeux">
                                <input type='hidden' name='action' value='form_ajouter'>
                                <input type="submit"  class="btn btn-primary btn-sm rounded mb-1"   name="ajouter" value="Ajouter">
                            </form>
                        </div>

                    </div>

                    <table class="table  ml-5">
                        <thead class="">
                            <tr>
                                <th>
                                    Identifiant
                                </th>
                                <th>
                                    Type jeux
                                </th>

                            </tr>
                        </thead>

                        <tbody>

                            {foreach from=$listeTypeJeux item=typeJeux}
                                <tr>
                                    <td>
                                        {$typeJeux.idTypeJeux}
                                    </td>
                                    <td>
                                        {$typeJeux.libelleTypeJeux}
                                    </td>

                                    <td>

                                        <div class="btn-group">
                                            <form action='index.php' method='post'>
                                                <input type='hidden' name='idTypeJeux' value='{$typeJeux.idTypeJeux}'>
                                                <input type='hidden' name='gestion' value='typeJeux'>
                                                <input type='hidden' name='action' value='form_modifier'>

                                                <input type="submit"  class="btn btn-primary btn-sm rounded mb-1 mr-2"   name="modifier" value="Modifier">
                                            </form>

                                            <form action='index.php' method='post'>
                                                <input type='hidden' name='idTypeJeux' value='{$typeJeux.idTypeJeux}'>
                                                <input type='hidden' name='gestion' value='typeJeux'>
                                                <input type='hidden' name='action' value='form_supprimer'>

                                                <input type="submit"  class="btn btn-primary btn-sm rounded mb-1"   name="supprimer" value="Supprimer">
                                            </form>

                                        </div>
                                    </td>
                                </tr>
                            {foreachelse}
                                <tr>
                                    <td colspan='6'>Aucun enregistrement de trouvé.</td>
                                </tr>
                            {/foreach}

                        </tbody>
                    </table>
                </div>
            </div>



        </div>

        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>

<?php

/**
 * Description of lieuModele
 *
 * @author tvosgiens
 */
class PegiJeuxModele extends Modele {

    private $parametre;

    public function __construct($parametre) {

        $this->parametre = $parametre;
    }

    public function getListePegiJeux() {

        //Requête attendue de type SELECT (liste des typejeux)
        $sql = "SELECT * FROM pegi ";

        $idRequete = $this->executeQuery($sql);

        return $idRequete->fetchall(PDO::FETCH_ASSOC);
    }

    public function getUnPegiJeu() {

        //Requête attendue de type SELECT (un seul lieu)
        $sql = "SELECT * FROM pegi WHERE idPegi = ?";

        $idRequete = $this->executeQuery($sql, array($this->parametre['idPegi']));

        //var_dump($idRequete->fetch());
        $pegiJeu = new PegiJeuxTable($idRequete->fetch());

        return $pegiJeu;
    }

    public function addPegiJeux(PegiJeuxTable $valeurs) {
        // Requête de type Insert (création)
        $sql = "INSERT INTO pegi (agePegi)
				  VALUES (?)";

        $idRequete = $this->executeQuery($sql, array(
            $valeurs->getAgePegi()
        ));

        if ($idRequete) {
            PegiJeuxTable::setMessageSucces("Création effectuée avec succès !");
        }
    }

    public function editPegiJeux(PegiJeuxTable $valeurs) {
        // Requête de type Insert (création)
        $sql = "UPDATE pegi SET agePegi = ?
			 WHERE idPegi = ?";


        $idRequete = $this->executeQuery($sql, array(
            $valeurs->getAgePegi(),
            $valeurs->getIdPegi()
        ));

        if ($idRequete) {
            PegiJeuxTable::setMessageSucces("Modification effectuée avec succès !");
        }
    }

    public function deletePegiJeux() {

        $sql = "DELETE FROM pegi WHERE idPegi = ?";


        $idRequete = $this->executeQuery($sql, array($this->parametre['idPegi']));

        if ($idRequete) {

            PegiJeuxTable::setMessageSucces("Suppression effectuée avec succès !");
        }
    }

}

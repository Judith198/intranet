<?php

/**
 * Description of lieuControleur
 *
 * @author tvosgiens
 */
class PegiJeuxControleur {

    private $parametre; //array
    private $oModele; // objet
    private $oVue; // objet

    public function __construct($parametre) {

        $this->parametre = $parametre;
//Création d'un objet modele
        $this->oModele = new PegiJeuxModele($this->parametre);
//Création d'un objet vue
        $this->oVue = new PegiJeuxVue($this->parametre);
    }

    public function liste() {

        $valeurs = $this->oModele->getListePegiJeux();

        $this->oVue->genererAffichageListe($valeurs);
    }

    public function form_consulter() {

        $valeurs = $this->oModele->getUnPegiJeu();

        $this->oVue->genererAffichageFiche($valeurs);
    }

    public function form_ajouter() {

        $prepareJeu = new PegiJeuxTable();

        $this->oVue->genererAffichageFiche($prepareJeu);
    }

    public function form_modifier() {

        $valeurs = $this->oModele->getUnPegiJeu();

        $this->oVue->genererAffichageFiche($valeurs);
    }

    public function form_supprimer() {

        $valeurs = $this->oModele->getUnPegiJeu();

        $this->oVue->genererAffichageFiche($valeurs);
    }

    public function ajouter() {

        $controlePegiJeux = new PegiJeuxTable($this->parametre);

        if ($controlePegiJeux->getAutorisationBD() == false) {
// ici nous sommes en erreur
            $this->oVue->genererAffichageFiche($controlePegiJeux);
        } else {
// ici l'insertion est possible !
            $this->oModele->addPegiJeux($controlePegiJeux);
//Ici l'objet controleur (oControleur)
//Il a été créé dans le routeur
            $this->liste();
        }
    }

    public function modifier() {

        $controlePegiJeux = new PegiJeuxTable($this->parametre);

        if ($controlePegiJeux->getAutorisationBD() == false) {
// ici nous sommes en erreur
            $this->oVue->genererAffichageFiche($controlePegiJeux);
        } else {
// ici l'édition est possible !
            $this->oModele->editPegiJeux($controlePegiJeux);
//Ici l'objet controleur (oControleur)
//Il a été créé dans le routeur

            $this->liste();
        }
    }

    public function supprimer() {

        $this->oModele->deletePegiJeux();

        $this->liste();
    }

}

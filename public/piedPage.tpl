<!-- Footer -->
<footer class="bg-dark fixed-bottom mx-0">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Bibliogames 2020</p>
    </div>
    <!-- /.container -->
</footer>
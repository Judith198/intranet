<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Bibliogames</title>

        <!-- Bootstrap core CSS -->
        <!--<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">  -->

        <!-- Custom styles for this template -->
        <!-- <link href="css/heroic-features.css" rel="stylesheet">-->

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />

    </head>
</head>

<body>
    {if $role eq 'Administrateur'}
        {include file="public/menu_Administrateur.tpl"}
    {/if}
    {if $role eq 'Modérateur'}
        {include file="public/menu_Moderateur.tpl"}
    {/if}
    {if $role eq 'Utilisateur'}
        {include file="public/menu_Utilisateur.tpl"}
    {/if}

    <!-- Page Content -->
    <div class=".container-fluid">
        <div class="esp"></div>
        <!-- Jumbotron Header -->
        <header class="jumbotron my-3 justify-content-center">
            <h1 class="display-3 text-center">Bienvenue sur Bibliogames !</h1>
            <h3 class="text-center">Retrouvez tous les jeux disponibles en un clic!</h3>
            <h5 class="ml4 text-center mr-4">
                <span class="letters letters-1">Prêt,</span>
                <span class="letters letters-2">feu,</span>
                <span class="letters letters-3">Trouvez!</span>
            </h5>

            {*  <a href="#" class="btn btn-primary btn-lg">Call to action!</a>*}
        </header>
    </div>
    <!-- /.row -->
    <div id="main_content" class="d-flex flex-row">
        <div id='main_card'>
            <div class="card text-white bg-primary mb-5" style="max-width: 18rem;">
                <div class="card-header">Tri par plateforme</div>
                <div class="card-body">
                    <h6 class="card-title">PC</h6>
                    <h6 class="card-title">Playstation 4</h6>
                    <h6 class="card-title">Xbox</h6>
                    <h6 class="card-title">Nintendo Switch</h6>

                </div>
            </div>
            <div class="card text-white bg-primary mb-5" style="max-width: 18rem;">
                <div class="card-header">Type</div>
                <div class="card-body">
                    <h6 class="card-title">MMORPG</h6>
                    <h6 class="card-title">RPG</h6>
                    <h6 class="card-title">FPS</h6>
                    <h6 class="card-title">Jeu de plateforme</h6>

                </div>
            </div>
            <div class="card text-white bg-primary mb-5" style="max-width: 18rem;">
                <div class="card-header">PEGI</div>
                <div class="card-body">
                    <h6 class="card-title">18</h6>
                    <h6 class="card-title">16</h6>
                    <h6 class="card-title">12</h6>
                    <h6 class="card-title">Tous public</h6>
                </div>
            </div>
        </div>
        <div class="container my-5 d-flex justify-content-center flex-column text-center shadow-lg p-3 mb-5 bg-white rounded">
            <div> <img src="./public/images/bibliogames_logo.png" id="logo" class=""></div>
            <form class="" action="/recherche/" method="get">
                <fieldset>
                    <div class="input-group">
                        <input id="oSaisie" name="oSaisie" type="text" class="form-control" placeholder="Veuillez saisir le nom d'un jeu">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">Recherche</button>
                        </div>
                    </div>
                </fieldset>
            </form>
            <p class="my-5"> ou </p>
            <div> <a href="index.php?gestion=jeux"><button type="button" class="btn btn-primary my-4">Accéder à la bibliothèque complète de jeux </button></a></div>
        </div>
    </div>
    <!-- /.container -->
    {include file = "public/piedPage.tpl"}


    <!-- Bootstrap core JavaScript -->
    <!-- <script src="vendor/jquery/jquery.min.js"></script>
     <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="public/js/custom.js" type="text/javascript"></script>

</body>

</html>

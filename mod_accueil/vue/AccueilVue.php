<?php

class AccueilVue {

    private $tpl; //objet smarty

    function __construct() {
        $this->tpl = new Smarty();
    }

    public function chargementValeurs() {
        $this->tpl->assign('titre', 'Bibliogames');
        $this->tpl->assign('deconnexion', 'Déconnexion');
        $this->tpl->assign('profil', 'Profil');
        $this->tpl->assign('role', $_SESSION['role']);
        $this->tpl->assign('nomPrenom', $_SESSION['prenomNom']);
    }

    function genererAffichage() {

        $this->chargementValeurs();
        $this->tpl->display('mod_accueil/vue/accueilVue.tpl');
    }

}

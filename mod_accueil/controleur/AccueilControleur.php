<?php

class AccueilControleur {

    function __construct() {
        $this->oVue = new AccueilVue();
    }

    function liste() {
        $this->oVue->genererAffichage();
    }

}

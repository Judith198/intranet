<?php

class Autoloader {

    public static function inscrire() {

        spl_autoload_register(array(__CLASS__, 'autoload')); //Pour le chargement automatique des class
    }

    public static function autoload($maclass) {
// la variable maclass accepte le nom de la classe : Accueil par ex (routeur du module accueil) ou Reunion (routeur, controleur, modele.....)

        $chemins = array(
            'mod_accueil/',
            'mod_accueil/controleur/',
            'mod_accueil/modele/',
            'mod_accueil/vue/',
            'mod_authentification/',
            'mod_authentification/controleur/',
            'mod_authentification/modele/',
            'mod_authentification/vue/',
            'mod_utilisateur/',
            'mod_utilisateur/controleur/',
            'mod_utilisateur/modele/',
            'mod_utilisateur/vue/',
            'mod_jeux/',
            'mod_jeux/controleur/',
            'mod_jeux/modele/',
            'mod_jeux/vue/',
            'mod_pegiJeux/',
            'mod_pegiJeux/controleur/',
            'mod_pegiJeux/modele/',
            'mod_pegiJeux/vue/',
            'mod_plateformes/',
            'mod_plateformes/controleur/',
            'mod_plateformes/modele/',
            'mod_plateformes/vue/',
            'mod_typeJeux/',
            'mod_typeJeux/controleur/',
            'mod_typeJeux/modele/',
            'mod_typeJeux/vue/'
        );

        foreach ($chemins as $chemin) {
            if (file_exists($chemin . $maclass . '.php')) {
                require_once ($chemin . $maclass . '.php');
                return;
            }
        }
    }

}
